﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace IoTAppBackend.Util
{
    class SQLConnection
    {
        internal static string GetSqlConnectionString()
        {
            // Prepare the connection string to Azure SQL Database.  
            var sqlConnectionSB = new SqlConnectionStringBuilder();
            
            sqlConnectionSB.DataSource = "tcp:v9imvtudr6.database.chinacloudapi.cn,1433"; //["Server"]  
            sqlConnectionSB.InitialCatalog = "iotappdb"; //["Database"]  

            sqlConnectionSB.UserID = "root@iotappdb@v9imvtudr6";  // "@yourservername"  as suffix sometimes.  
            sqlConnectionSB.Password = "iotappdb@16426";
            sqlConnectionSB.IntegratedSecurity = false;
            
            sqlConnectionSB.ConnectRetryCount = 3;
            sqlConnectionSB.ConnectRetryInterval = 10;
            
            sqlConnectionSB.IntegratedSecurity = false;
            sqlConnectionSB.Encrypt = true;
            sqlConnectionSB.ConnectTimeout = 30;

            return sqlConnectionSB.ToString();
        }
    }
}
