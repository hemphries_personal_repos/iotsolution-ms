﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoTAppBackend.Util
{
    public class Types
    {
        public enum MessageType
        {
            MT_DeviceInfo = 0,
            MT_Telemetry,
            MT_HeartBeat,
            MT_Command
        }

        public enum TelemetryType
        {
            TT_Temperature = 0,
            TT_Humidity
        }
    }
}
