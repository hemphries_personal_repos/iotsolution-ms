﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace IoTAppBackend.Util
{
    class ImageHelper
    {
        class PhotoInformation
        {
            public DateTime datetime;
            public bool status;
            public string photoID;
            public string content;

            public JObject Serialize()
            {
                JObject obj = new JObject();

                string pattern = "MM-dd HH:mm:ss";
                obj.Add("datetime", datetime.ToString(pattern));
                obj.Add("status", status);
                obj.Add("photo_id", photoID);
                obj.Add("content", content);
                return obj;
            }

        }


        private static ImageHelper helper = null;
        private string subscriptionKey = "2053b6dddb9841a3afb5140cd745f61d";
        private ArrayList trustList = null;
        private Queue visitors = null;
        private int capacity;
        private float trustThreshold;

        private ImageHelper()
        {
            trustList = new ArrayList();
            visitors = new Queue();
            capacity = 10;
            trustThreshold = 0.5f;
        }

        public static ImageHelper Instance()
        {
            if(helper == null)
            {
                helper = new ImageHelper();
            }
            return helper;
        }


        public async Task<bool> VerifyVisitor(string visitor)
        {
            if(trustList.Count == 0)
            {
                return false;
            }

            string visitorFaceId = await RequestFaceID(visitor);
            int size = trustList.Count;
            TimeSpan now = DateTime.Now.TimeOfDay;

            string[] trustID = new string[size];
            int i = 0;
            foreach (PhotoInformation info in trustList)
            {
                TimeSpan trustTime = DateTime.Now - info.datetime;
                if((now - trustTime).TotalHours > 24)
                {
                    info.photoID = await RequestFaceID(info.content);
                    info.datetime = DateTime.Now;
                }
                trustID[i] = info.photoID;
                i += 1;
            }


            bool result = await FindSimilarity(trustID, visitorFaceId);
            PhotoInformation newVisitorInfo = new PhotoInformation();
            while(visitors.Count >= this.capacity)
            {
                visitors.Dequeue();
            }
            newVisitorInfo.datetime = DateTime.Now;
            newVisitorInfo.content = visitor;
            newVisitorInfo.photoID = visitorFaceId;
            newVisitorInfo.status = result;
            visitors.Enqueue(newVisitorInfo);
            return result;

        }

        public async Task<bool> AddVisitorToTrustList(string visitor)
        {
            string visitorFaceId = await RequestFaceID(visitor);

            if(visitorFaceId == null)
            {
                return false;
            }

            PhotoInformation trustInfo = new PhotoInformation();

            trustInfo.content = visitor;
            trustInfo.photoID = visitorFaceId;
            trustInfo.datetime = DateTime.Now;
            trustInfo.status = true;
            trustList.Add(trustInfo);
            return true;
        }

        public async Task<JArray> RecentVisitor(int offset)
        {
            JObject recentVisitors = new JObject();

            JArray jsonVisitors = new JArray();

            foreach(PhotoInformation info in visitors)
            {
                JObject obj = info.Serialize();
                jsonVisitors.Add(obj);
            }

           // recentVisitors.Add("visitors", jsonVisitors);

            return jsonVisitors;
        }

        public async Task<JArray> TrustedVisitor(int offset)
        {
            JObject recentVisitors = new JObject();

            JArray jsonVisitors = new JArray();
            foreach(PhotoInformation info in trustList)
            {
                JObject tmp = info.Serialize();
                jsonVisitors.Add(tmp);
            }

    
            //recentVisitors.Add("trust_visitor", jsonVisitors);

            return jsonVisitors;
        } 


        private async Task<bool> FindSimilarity(string[] faceMaster, string faceVisitor)
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            // Request headers
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionKey);

            var uri = "https://api.projectoxford.ai/face/v1.0/findsimilars?" + queryString;

            HttpResponseMessage response;

            JObject body = new JObject();
            body.Add("faceId", faceVisitor);
            JArray array = new JArray(faceMaster);
            body.Add("faceIds", array);

            // Request body
            byte[] byteData = Encoding.UTF8.GetBytes(body.ToString());
            Stream rs = null;
            using (var content = new ByteArrayContent(byteData))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                response = await client.PostAsync(uri, content);
                if (!response.IsSuccessStatusCode)
                {
                    return false;
                }
                rs = response.Content.ReadAsStreamAsync().Result;
            }

            var encoding = ASCIIEncoding.ASCII;
            using (var reader = new System.IO.StreamReader(rs, encoding))
            {
                string responseText = reader.ReadToEnd();
                JArray json = JArray.Parse(responseText);
                double confident = 0.0;
                foreach(var obj in json)
                {
                    confident = (double)obj["confidence"];
                    if(confident >= this.trustThreshold)
                    {
                        return true;
                    }
                }

            }
            return false;

        }

        //get faceID
        public async Task<string> RequestFaceID(string imgBase64)
        {
            byte[] byteData = Convert.FromBase64String(imgBase64);
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            // Request headers
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionKey);

            // Request parameters
            queryString["returnFaceId"] = "true";
            queryString["returnFaceLandmarks"] = "false";
  
            var uri = "https://api.projectoxford.ai/face/v1.0/detect?" + queryString;

            HttpResponseMessage response;
            Stream rs = null;
            // Request body

            using (var content = new ByteArrayContent(byteData))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                response = await client.PostAsync(uri, content);
                if(!response.IsSuccessStatusCode)
                {
                    return null;
                }
                rs = response.Content.ReadAsStreamAsync().Result;
            }
            var encoding = ASCIIEncoding.ASCII;
            using (var reader = new System.IO.StreamReader(rs, encoding))
            {
                string responseText = reader.ReadToEnd();
                JArray jsonArray = JArray.Parse(responseText);
                
                if(jsonArray.Count == 1)
                {
                    JToken face = jsonArray.ElementAt(0);
                    string faceID = (string)face["faceId"];
                    return faceID;
                }
                else
                {
                    return null;
                }


            }
        }

    }
}
/*
    class ImageEncoder
    {
        public static byte[] Decode(string base64string)
        {
            return Convert.FromBase64String(base64string);
        }

        public static string Encode(byte[] img)
        {
            return Convert.ToBase64String(img);
        }


    }

*/





