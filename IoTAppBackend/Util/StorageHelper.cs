﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoTAppBackend.Util
{
    class StorageHelper
    {
        public const string GLOBAL_PARTITION_KEY = "GlobalPartitionKey";

        private const string endpointSuffix = "core.chinacloudapi.cn";
        private const string storageAccountName = "iotappstorage";
        private const string storageAccountKey = "M22PRG/1dhmOviftfddeSY/SsHkTO1kZ4aSYQxPAkoSF2YKUQzEULIygmKrFh40Qwbmu9rzMLrArSedxSvFlpg==";

        internal static CloudStorageAccount storageAccount;
        internal static CloudTableClient tableClient;
        internal static CloudTable deviceInfo;

        internal static CloudTable GetDeviceInfoTable()
        {
            if (deviceInfo != null)
                return deviceInfo;

            if (storageAccount == null)
            {
                string storageConnectionString = string.Format("DefaultEndpointsProtocol=https;AccountName={0};" +
                    "AccountKey={1};EndpointSuffix={2}", storageAccountName, storageAccountKey, endpointSuffix);
                storageAccount = CloudStorageAccount.Parse(storageConnectionString);
            }
            if (tableClient == null)
                tableClient = storageAccount.CreateCloudTableClient();

            deviceInfo = tableClient.GetTableReference("DeviceInfo");
            deviceInfo.CreateIfNotExists();

            return deviceInfo;
        }
    }
}
