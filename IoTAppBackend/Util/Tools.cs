﻿using IoTAppBackend.Models;
using Microsoft.WindowsAzure.ServiceRuntime;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoTAppBackend.Util
{
    class Tools
    {
        private static string connectionString = "HostName=iotappih.azure-devices.cn;SharedAccessKeyName" + 
            "=iothubowner;SharedAccessKey=pecUVSeO8EthsWvPt0re/skf2yr5+Kjd7tCuvs5U3l8=";

        private static string iotHubUri = "iotappih.azure-devices.cn";

        private static string telemetryWorkerRoleAddress;

        //*************************************************************************************************
        // Methods to get objects.
        //*************************************************************************************************

        public static JObject GetResponseMessageObject(int errorCode)
        {
            JObject msgObj = new JObject();
            msgObj.Add("error_code", errorCode);
            msgObj.Add("error_message", "");

            return msgObj;
        }

        public static JObject GetCommandObject(Command cmd)
        {
            JObject cmdObj = new JObject();
            cmdObj.Add("device_id", cmd.device_id);
            cmdObj.Add("message_type", (int)Types.MessageType.MT_Command);
            cmdObj.Add("command_name", cmd.command_name);
            cmdObj.Add("command_value", cmd.command_value);

            return cmdObj;
        }

        public static JObject GetRegistryInfoObject(int errorCode, string deviceKey, string hubUri)
        {
            JObject regObj = new JObject();
            regObj.Add("error_code", errorCode);
            regObj.Add("error_message", "");
            regObj.Add("token", deviceKey);
            regObj.Add("iothub_name", hubUri);

            return regObj;
        }

        //*************************************************************************************************
        // Methods to get strings.
        //*************************************************************************************************

        public static string GetIoTHubConnectionString()
        {
            return connectionString;
        }

        public static string GetIoTHubUri()
        {
            return iotHubUri;
        }

        public static string GetTelemetryWorkerRoleAddress()
        {
            if (telemetryWorkerRoleAddress == null)
            {
                var endpoint = RoleEnvironment.Roles["TelemetryWorkerRole"].Instances[0].
                    InstanceEndpoints["Endpoint1"];
                telemetryWorkerRoleAddress = string.Format("http://{0}", endpoint.IPEndpoint);
            }

            return telemetryWorkerRoleAddress;
        }

        public static int GetTelemetryWorkerRoleAddressPort()
        {
            return RoleEnvironment.Roles["TelemetryWorkerRole"].Instances[0].
                InstanceEndpoints["Endpoint1"].IPEndpoint.Port;
        }
    }
}
