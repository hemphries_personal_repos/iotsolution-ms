﻿using IoTAppBackend.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace IoTAppBackend.Controllers
{
    public class VisitorController : ApiController
    {
        // GET: api/Visitor
        public async Task<JArray> Get()
        {
            ImageHelper helper = ImageHelper.Instance();

            JArray result = await helper.RecentVisitor(0);

            return result;
        }

        [HttpPost]
        // POST: api/Visitor
        public async Task<JObject> Post()
        {
            string raw = await Request.Content.ReadAsStringAsync();
            // string imgString = obj;
            JObject obj = JObject.Parse(raw);
            string pic = (string)obj["photo"];

            bool result = await ImageHelper.Instance().VerifyVisitor(pic);
            JObject rs = new JObject();
            if (result)
            {
                rs.Add("error_code", -1);
                rs.Add("error_message", "");
                rs.Add("result", "Approved");
            }
            else
            {
                rs.Add("error_code", 0);
                rs.Add("error_message", "Confidence below threshold");
                rs.Add("result", "Deny");
            }
            return rs;
        }

        // PUT: api/Visitor/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Visitor/5
        public void Delete(int id)
        {
        }
    }
}
