﻿using IoTAppBackend.Models;
using IoTAppBackend.Util;
using Microsoft.Azure.Devices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using Newtonsoft.Json.Linq;
using Microsoft.WindowsAzure.Storage.Table;
using System.Diagnostics;

namespace IoTAppBackend.Controllers
{
    public class CommandsController : ApiController
    {
        private static ServiceClient serviceClient;

        [HttpPost]
        public async Task<JObject> SendCommandToDevice([FromBody]Command cmd)
        {
            if (serviceClient == null)
                serviceClient = ServiceClient.CreateFromConnectionString(
                    Tools.GetIoTHubConnectionString());

            var messageString = JsonConvert.SerializeObject(Tools.GetCommandObject(cmd));
            var message = new Message(Encoding.ASCII.GetBytes(messageString));
            
            await serviceClient.SendAsync(cmd.device_id, message);

            UpdateDeviceInfoIfNeeded(cmd);
            
            return Tools.GetResponseMessageObject(-1);
        }

        private void UpdateDeviceInfoIfNeeded(Command cmd)
        {
            if (cmd.command_name.Equals("ChangeState"))
            {
                CloudTable deviceInfo = StorageHelper.GetDeviceInfoTable();
                TableOperation retrieveOperation = TableOperation.Retrieve<DeviceInfoTableEntity>(
                    DeviceInfoTableEntity.GLOBAL_PARTITION_KEY, cmd.device_id);
                TableResult retrievedResult = deviceInfo.Execute(retrieveOperation);
                DeviceInfoTableEntity updateEntity = (DeviceInfoTableEntity)retrievedResult.Result;
                if (updateEntity != null)
                {
                    updateEntity.Extra = cmd.command_value;

                    TableOperation updateOperation = TableOperation.Replace(updateEntity);
                    deviceInfo.Execute(updateOperation);
                    Trace.TraceInformation("DeviceInfo: {0} updated.", cmd.device_id);
                }
                else
                {
                    Trace.TraceInformation("DeviceInfo: {0} could not be retrieved.", cmd.device_id);
                }
            }
        }
    }
}