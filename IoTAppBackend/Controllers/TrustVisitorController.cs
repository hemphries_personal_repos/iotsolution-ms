﻿using IoTAppBackend.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace IoTAppBackend.Controllers
{
    public class TrustVisitorController : ApiController
    {
        // GET: api/TrustVisitor
        public async Task<JArray> Get()
        {
            ImageHelper helper = ImageHelper.Instance();

            JArray result = await helper.TrustedVisitor(0);

            return result;
        }

        // GET: api/TrustVisitor/5
        public string Get(int id)
        {
            return "value";
        }

        [HttpPost]
        // POST: api/TrustVisitor
        public async Task<JObject> Post()
        {
            string raw = await Request.Content.ReadAsStringAsync();
            // string imgString = obj;
            JObject obj = JObject.Parse(raw);
            bool result = await ImageHelper.Instance().AddVisitorToTrustList((string)obj["photo"]);
            JObject rs = new JObject();
            if (result)
            {
                rs.Add("error_code", -1);
                rs.Add("error_message", "");
            }
            else
            {
                rs.Add("error_code", 0);
                rs.Add("error_message", "sth wrong");
            }
            return rs;

        }

        // PUT: api/TrustVisitor/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/TrustVisitor/5
        public void Delete(int id)
        {
        }
    }
}
