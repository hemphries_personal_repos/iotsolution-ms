﻿using IoTAppBackend.Util;
using IoTAppBackend.Models;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Common.Exceptions;
using Microsoft.WindowsAzure.ServiceRuntime;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace IoTAppBackend.Controllers
{
    public class DevicesController : ApiController
    {
        

        private static RegistryManager registryManager;
        
        // GET devices
        [HttpGet]
        public JArray GetAllDevicesInfo()
        {
            CloudTable deviceInfo = StorageHelper.GetDeviceInfoTable();

            // Retrieve all entities in a partition
            TableQuery<DeviceInfoTableEntity> query = new TableQuery<DeviceInfoTableEntity>().
                Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, 
                DeviceInfoTableEntity.GLOBAL_PARTITION_KEY));

            JArray result = new JArray();
            foreach (DeviceInfoTableEntity entity in deviceInfo.ExecuteQuery(query))
            {
                JObject obj = new JObject();
                obj.Add("device_id", entity.RowKey);
                obj.Add("device_type", entity.DeviceType);
                obj.Add("extra", entity.Extra);
                result.Add(obj);
            }

            return result;
        }

        // GET devices/deviceKey
        [HttpGet]
        public string GetDeviceInfo(string deviceId)
        {
            return "deviceInfo";
        }

        // POST devices
        [HttpPost]
        public async Task<JObject> GetDeviceKey([FromBody]RegistryRequest request)
        {
            Device device;
            if (registryManager == null)
            {
                registryManager = RegistryManager.CreateFromConnectionString(
                    Tools.GetIoTHubConnectionString());
            }

            try
            {
                device = await registryManager.AddDeviceAsync(new Device(request.device_id));
            }
            catch (DeviceAlreadyExistsException)
            {
                device = await registryManager.GetDeviceAsync(request.device_id);
            }

            return Tools.GetRegistryInfoObject(-1, device.Authentication.SymmetricKey.PrimaryKey,
                Tools.GetIoTHubUri());
        }
    }
}
