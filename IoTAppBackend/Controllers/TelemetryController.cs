﻿using IoTAppBackend.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace IoTAppBackend.Controllers
{
    public class TelemetryController : ApiController
    {
        private SqlConnection sqlConn;

        [HttpGet]
        public JArray GetHistoryTelemetry([FromUri] string device_id, 
            [FromUri] string telemetry_type)
        {
            if (sqlConn == null)
            {
                sqlConn = new SqlConnection(SQLConnection.GetSqlConnectionString());
                sqlConn.Open();
            }

            JArray result = telemetry_type.Equals("" + Types.TelemetryType.TT_Temperature) ? 
                getTemperatures(device_id) : getHumidities(device_id);

            return result;
        }

        private JArray getTemperatures(string device_id)
        {
            JArray result = new JArray();
            using (var dbCommand = sqlConn.CreateCommand())
            {
                dbCommand.CommandText = @"
                SELECT TOP 12 [utc_time], [temperature]
                FROM [dbo].[Temperature]
                WHERE [device_id]=" + device_id +
                "ORDER BY utc_time DESC";

                var dataReader = dbCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    JObject obj = new JObject();
                    obj.Add("time", dataReader.GetString(0));
                    obj.Add("temperature", dataReader.GetString(1));

                    result.Add(obj);
                }
            }

            return result;
        }

        private JArray getHumidities(string device_id)
        {
            JArray result = new JArray();
            using (var dbCommand = sqlConn.CreateCommand())
            {
                dbCommand.CommandText = @"
                SELECT TOP 12 [utc_time], [humidity]
                FROM [dbo].[Humidity]
                WHERE [device_id]=" + device_id +
                "ORDER BY utc_time DESC";
                
                var dataReader = dbCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    JObject obj = new JObject();
                    obj.Add("time", dataReader.GetString(0));
                    obj.Add("humidity", dataReader.GetString(1));

                    result.Add(obj);
                }
            }

            return result;
        }
    }
}
