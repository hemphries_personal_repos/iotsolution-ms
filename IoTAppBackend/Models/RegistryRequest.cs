﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoTAppBackend.Models
{
    public class RegistryRequest
    {
        public string device_id { get; set; }
        public string location { get; set; }
        public string description { get; set; }
    }
}
