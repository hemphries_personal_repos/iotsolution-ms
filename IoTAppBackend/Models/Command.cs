﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IoTAppBackend.Models
{
    public class Command
    {
        public string device_id { get; set; }
        public string command_name { get; set; }
        public string command_value { get; set; }
    }
}