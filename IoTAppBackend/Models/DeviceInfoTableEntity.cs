﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IoTAppBackend.Models
{
    public class DeviceInfoTableEntity : TableEntity
    {
        public const string GLOBAL_PARTITION_KEY = "GlobalPartitionKey";

        public const string CONNECTION_STATE_CONNECTED = "Connected";
        public const string CONNECTION_STATE_DISCONNECTED = "Disconnected";

        public const string STATE_ENABLE = "Enable";
        public const string STATE_DISABLE = "Disable";

        public DeviceInfoTableEntity() { }

        public DeviceInfoTableEntity(string deviceId, string deviceKey, string deviceType, 
            string deviceLocation, string deviceDescription, string firmwareVersion, 
            string hardwareVersion, string manufacturer, string serialnumber, 
            string defaultMaxPeriod, string extra)
        {
            this.PartitionKey = GLOBAL_PARTITION_KEY;
            this.RowKey = deviceId;

            this.DeviceType = deviceType;
            this.DeviceKey = deviceKey;
            this.DeviceLocation = deviceLocation;
            this.DeviceDescription = deviceDescription;
            this.FirmwareVersion = firmwareVersion;
            this.HardwareVersion = hardwareVersion;
            this.Manufacturer = manufacturer;
            this.Serialnumber = serialnumber;
            this.DefaultMaxPeriod = defaultMaxPeriod;
            this.Extra = extra;
        }

        /**
         * The static device info.
         **/
        public string DeviceKey { get; set; }
        public string DeviceType { get; set; }
        public string DeviceLocation { get; set; }
        public string DeviceDescription { get; set; }
        public string FirmwareVersion { get; set; }
        public string HardwareVersion { get; set; }
        public string Manufacturer { get; set; }
        public string Serialnumber { get; set; }
        public string DefaultMaxPeriod { get; set; }
        // Value of the device.
        public string Extra { get; set; }

        /**
         * The runtime device info. 
         **/
        public string ConnectionState { get; set; }
        public string State { get; set; }
        public string LastActiveTime { get; set; }
    }
}
