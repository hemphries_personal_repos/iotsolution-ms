using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.ServiceBus.Messaging;

namespace TelemetryWorkerRole
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        private EventProcessorHost eventProcessorHost;

        private const string eventHubConnectionString = "Endpoint=sb://iotappsb.servicebus.chinacloudapi.cn/;" + 
            "SharedAccessKeyName=eventhubowner;SharedAccessKey=LXxgV/VFWpr6b8TVINyeqhgcPax9tmES9uQtvFOymnY=";
        private const string eventHubName = "realtimedataeh";

        // An undocumented property!
        private const string endpointSuffix = "core.chinacloudapi.cn";
        private const string storageAccountName = "iotappstorage";
        private const string storageAccountKey = "M22PRG/1dhmOviftfddeSY/SsHkT" +
            "O1kZ4aSYQxPAkoSF2YKUQzEULIygmKrFh40Qwbmu9rzMLrArSedxSvFlpg==";
        
        public override void Run()
        {
            Trace.TraceInformation("TelemetryWorkerRole is running");
            
            try
            {
                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;
            
            bool result = base.OnStart();

            Trace.TraceInformation("TelemetryWorkerRole has been started");

            string storageConnectionString = string.Format("DefaultEndpointsProtocol=https;AccountName={0};" + 
                "AccountKey={1};EndpointSuffix={2}", storageAccountName, storageAccountKey, endpointSuffix);
            eventProcessorHost = new EventProcessorHost(RoleEnvironment.CurrentRoleInstance.Id, eventHubName, 
                EventHubConsumerGroup.DefaultGroupName, eventHubConnectionString, storageConnectionString);

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("TelemetryWorkerRole is stopping");

            eventProcessorHost.UnregisterEventProcessorAsync().Wait();
            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("TelemetryWorkerRole has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            var options = new EventProcessorOptions();
            options.ExceptionReceived += (sender, e) => { Trace.TraceInformation(e.Exception.ToString()); };
            await eventProcessorHost.RegisterEventProcessorAsync<TelemetryProcessor>(options);
            
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("TelemetryWorkerRole is working");
                await Task.Delay(10000);
            }
        }
    }
}
