﻿using IoTAppBackend.Util;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelemetryWorkerRole.util
{
    class RedisCacheConnection
    {
        internal static Lazy<ConnectionMultiplexer> lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
        {
            return ConnectionMultiplexer.Connect("iotappcache.redis.cache.chinacloudapi.cn," +
                "abortConnect=false,ssl=true,password=T/dKiyPkOXNeL1hfCE9wKxsvjK+aZtViwJzz5+EC4S0=");
        });

        internal static ConnectionMultiplexer Connection
        {
            get
            {
                return lazyConnection.Value;
            }
        }

        internal static string GetCacheKey(string prefix, string suffix, Types.TelemetryType type)
        {
            return prefix + "_" + suffix + "_" + type;
        }
    }
}
