﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using IoTAppBackend.Util;
using TelemetryWorkerRole.handler;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;

namespace TelemetryWorkerRole
{
    class TelemetryProcessor : IEventProcessor
    {
        Stopwatch checkpointStopWatch;

        private TemperatureHandler mTemperatureHandler;
        private HumidityHandler mHumidityHandler;

        private HttpListener listener;

        public TelemetryProcessor()
        {
            mTemperatureHandler = new TemperatureHandler();
            mHumidityHandler = new HumidityHandler();
            
            // This task should not be started in production environment, because it's not 
            // a scalable architecture.
            // listener = new HttpListener();
            // Task.Run(() => StartHttpListener());
        }

        async Task IEventProcessor.CloseAsync(PartitionContext context, CloseReason reason)
        {
            if (reason == CloseReason.Shutdown)
            {
                await context.CheckpointAsync();
            }
        }

        Task IEventProcessor.OpenAsync(PartitionContext context)
        {
            this.checkpointStopWatch = new Stopwatch();
            this.checkpointStopWatch.Start();
            
            return Task.FromResult<object>(null);
        }

        async Task IEventProcessor.ProcessEventsAsync(PartitionContext context, IEnumerable<EventData> messages)
        {
            foreach (EventData eventData in messages)
            {
                string data = Encoding.UTF8.GetString(eventData.GetBytes());
                try
                {
                    JObject obj = JObject.Parse(data);
                    int msgType = Int32.Parse(obj.GetValue("telemetry_type").ToString());
                    if (msgType == (int)Types.TelemetryType.TT_Temperature)
                    {
                        mTemperatureHandler.HandleMassge(obj);
                    }
                    else if (msgType == (int)Types.TelemetryType.TT_Humidity)
                    {
                        mHumidityHandler.HandleMassge(obj);
                    }
                }
                catch (Exception e)
                {
                    Trace.TraceInformation(e.ToString());
                }
            }

            //Call checkpoint every 5 minutes, so that worker can resume processing from 5 minutes back if it restarts.
            if (this.checkpointStopWatch.Elapsed > TimeSpan.FromMinutes(5))
            {
                await context.CheckpointAsync();
                this.checkpointStopWatch.Restart();
            }
        }

        public static bool PortInUse(int port)
        {
            bool inUse = false;

            IPGlobalProperties ipProperties = IPGlobalProperties.GetIPGlobalProperties();
            IPEndPoint[] ipEndPoints = ipProperties.GetActiveTcpListeners();
            
            foreach (IPEndPoint endPoint in ipEndPoints)
            {
                if (endPoint.Port == port)
                {
                    inUse = true;
                    break;
                }
            }
            
            return inUse;
        }

        // This listener thread should only be used for debug.
        private void StartHttpListener()
        {
            try {
                if (!PortInUse(8081))
                {
                    listener.Prefixes.Add("http://+:8081/");
                    listener.Start();
                }
            }
            catch (Exception e)
            {
                Trace.TraceInformation(e.ToString());
            }

            while (true)
            {
                try {
                    var context = listener.GetContext();
                    HttpListenerRequest request = context.Request;
                    string device_id = request.QueryString.Get("device_id");
                    string telemetry_type = request.QueryString.Get("telemetry_type");

                    HttpListenerResponse response = context.Response;
                    string responseString = "[]";
                    if (telemetry_type.Equals("" + (int)Types.TelemetryType.TT_Temperature))
                    {
                        responseString = mTemperatureHandler.GetHistoricalValues(device_id);
                    }
                    else if (telemetry_type.Equals("" + (int)Types.TelemetryType.TT_Humidity))
                    {
                        responseString = mHumidityHandler.GetHistoricalValues(device_id);
                    }

                    byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
                    response.ContentLength64 = buffer.Length;
                    System.IO.Stream output = response.OutputStream;
                    output.Write(buffer, 0, buffer.Length);
                    output.Close();
                }
                catch (Exception e)
                {
                    Trace.TraceInformation(e.ToString());
                }
            }
        }
    }
}
