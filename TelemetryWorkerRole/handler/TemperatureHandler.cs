﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Collections.Concurrent;
using TelemetryWorkerRole.util;
using StackExchange.Redis;
using System.Data.SqlClient;
using IoTAppBackend.Util;

namespace TelemetryWorkerRole.handler
{
    class TemperatureHandler : TelemetryHandler
    {
        private Dictionary<string, CachedThermometer> mThermometers;
        private IDatabase cache;
        private SqlConnection sqlConn;

        public TemperatureHandler()
        {
            mThermometers = new Dictionary<string, CachedThermometer>();
            cache = RedisCacheConnection.Connection.GetDatabase();
            sqlConn = new SqlConnection(SQLConnection.GetSqlConnectionString());
            sqlConn.Open();
        }

        public override bool HandleMassge(JObject message)
        {
            bool flag = true;
            string deviceId = message.GetValue("device_id").Value<string>();
            long currentTime = GetCurrentUtcTimeInSeconds();

            CachedThermometer thermometer = null;
            if (mThermometers.ContainsKey(deviceId))
            {
                thermometer = mThermometers[deviceId];
            }
            else
            {
                thermometer = new CachedThermometer(deviceId, cache, sqlConn);
                mThermometers[deviceId] = thermometer;
            }

            // Update current temperature if needed.
            long msgUtcTime = GetUtcTimeOffset(DateTime.Parse(message.GetValue("EventEnqueuedUtcTime").Value<string>()));
            if (Math.Abs(msgUtcTime - currentTime) <= TIME_ACCEPTABLE_DELTA)
                thermometer.CurrentValue = message.GetValue("temperature").Value<double>();

            thermometer.Push(msgUtcTime, message.GetValue("temperature").Value<double>());

            return flag;
        }
        
        private class CachedThermometer : Sensor
        {
            private string deviceId;
            private IDatabase cache;
            private SqlConnection sqlConn;
            
            public double CurrentValue
            {
                get
                {
                    return (double)cache.StringGet(RedisCacheConnection.GetCacheKey(
                        deviceId, SUFFIX_CURRENT_VALUE, Types.TelemetryType.TT_Temperature));
                }

                internal set
                {
                    currentValue = value;
                    cache.StringSet(RedisCacheConnection.GetCacheKey(
                        deviceId, SUFFIX_CURRENT_VALUE, Types.TelemetryType.TT_Temperature), currentValue);
                }
            }

            public CachedThermometer(string deviceId, IDatabase cache, SqlConnection sqlConn)
            {
                this.deviceId = deviceId;
                this.cache = cache;
                this.sqlConn = sqlConn;
                
                if (cache.StringGet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_LAST_TIME_POINT, 
                    Types.TelemetryType.TT_Temperature)) == RedisValue.Null)
                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_LAST_TIME_POINT, 
                        Types.TelemetryType.TT_Temperature), lastTimePoint);
                if (cache.StringGet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_CURRENT_VALUE, 
                    Types.TelemetryType.TT_Temperature)) == RedisValue.Null)
                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_CURRENT_VALUE, 
                        Types.TelemetryType.TT_Temperature), currentValue);
                if (cache.StringGet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_PENDING_AVERAGE_VALUE, 
                    Types.TelemetryType.TT_Temperature)) == RedisValue.Null)
                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_PENDING_AVERAGE_VALUE, 
                        Types.TelemetryType.TT_Temperature), pendingAverageValue);
                if (cache.StringGet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_PENDING_COUNT, 
                    Types.TelemetryType.TT_Temperature))  == RedisValue.Null)
                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_PENDING_COUNT, 
                        Types.TelemetryType.TT_Temperature), pendingCount);
            }

            public void Push(long msgUtcTime, double value)
            {
                lastTimePoint = (long)cache.StringGet(RedisCacheConnection.GetCacheKey(
                    deviceId, SUFFIX_LAST_TIME_POINT, Types.TelemetryType.TT_Temperature));
                pendingAverageValue = (double)cache.StringGet(RedisCacheConnection.GetCacheKey(
                    deviceId, SUFFIX_PENDING_AVERAGE_VALUE, Types.TelemetryType.TT_Temperature));
                pendingCount = (int)cache.StringGet(RedisCacheConnection.GetCacheKey(
                    deviceId, SUFFIX_PENDING_COUNT, Types.TelemetryType.TT_Temperature));

                // The temperature value should be deprecated if it's emmitted time is 
                // event earlier than last time point.
                if (msgUtcTime < lastTimePoint)
                    return;

                if (msgUtcTime - lastTimePoint <= TIME_POINT_SPAN)
                {
                    pendingAverageValue = (pendingAverageValue * pendingCount + value) / 
                        (pendingCount + 1);
                    pendingCount++;

                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_PENDING_AVERAGE_VALUE, 
                        Types.TelemetryType.TT_Temperature), pendingAverageValue);
                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_PENDING_COUNT, 
                        Types.TelemetryType.TT_Temperature), pendingCount);
                }
                else
                {
                    if (pendingCount > 0)
                    {
                        using (var dbCommand = sqlConn.CreateCommand())
                        {

                            dbCommand.CommandText = @"
                                INSERT INTO Temperature
                                VALUES ('" + deviceId + "', '" + pendingAverageValue + "', '"+ lastTimePoint +"');";

                            dbCommand.ExecuteNonQuery();
                        }
                    }

                    pendingCount = 1;
                    pendingAverageValue = value;
                    lastTimePoint = msgUtcTime / TIME_POINT_SPAN * TIME_POINT_SPAN;

                    // Update values in cache.
                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_LAST_TIME_POINT, 
                        Types.TelemetryType.TT_Temperature), lastTimePoint);
                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_PENDING_AVERAGE_VALUE, 
                        Types.TelemetryType.TT_Temperature), pendingAverageValue);
                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_PENDING_COUNT, 
                        Types.TelemetryType.TT_Temperature), pendingCount);
                }
            }
        }
    }
}
