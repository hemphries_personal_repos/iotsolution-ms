﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace TelemetryWorkerRole.handler
{
    public abstract class TelemetryHandler : ITelemetryHandler
    {
        // Events' timestamp is 8 hours early than current time.
        protected static long TIME_BIAS = 8 * 3600;

        // An acceptable timespan in seconds from current time.
        protected static long TIME_ACCEPTABLE_DELTA = 60;

        public abstract bool HandleMassge(JObject message);

        protected static long GetCurrentUtcTimeInSeconds()
        {
            TimeSpan t = (DateTime.UtcNow - new DateTime(1970, 1, 1));
            return (long)t.TotalSeconds;
        }

        protected static long GetUtcTimeOffset(DateTime date)
        {
            return (long)(date - new DateTime(1970, 1, 1)).TotalSeconds + TIME_BIAS;
        }

        public abstract class Sensor
        {
            // Record average temperature per "TIME_POINT_SPAN".
            protected const long SECONDS_PER_HOUR = 3600;
            public static long TIME_POINT_SPAN = 10;
            public static int HISTORY_TIME_POINT_NUM = 12;

            // Suffixes used for generating cache keys.
            protected const string SUFFIX_CURRENT_VALUE = "CurrentValue";
            protected const string SUFFIX_LAST_TIME_POINT = "LastTimePoint";
            protected const string SUFFIX_PENDING_AVERAGE_VALUE = "PendingAverageValue";
            protected const string SUFFIX_PENDING_COUNT = "PendingCount";

            protected long lastTimePoint;
            protected double currentValue;
            protected double pendingAverageValue;
            protected int pendingCount;

            public Sensor()
            {
                this.currentValue = 0.0;
                this.pendingAverageValue = 0.0;
                this.pendingCount = 0;
                this.lastTimePoint = GetCurrentUtcTimeInSeconds() / TIME_POINT_SPAN * TIME_POINT_SPAN;
            }
        }
    }
}
