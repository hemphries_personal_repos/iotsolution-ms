﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using TelemetryWorkerRole.util;
using StackExchange.Redis;
using System.Data.SqlClient;
using IoTAppBackend.Util;

namespace TelemetryWorkerRole.handler
{
    class HumidityHandler : TelemetryHandler
    {
        private Dictionary<string, CachedHygrometer> mHygrometers;
        private IDatabase cache;
        private SqlConnection sqlConn;

        public HumidityHandler()
        {
            this.mHygrometers = new Dictionary<string, CachedHygrometer>();
            cache = RedisCacheConnection.Connection.GetDatabase();
            sqlConn = new SqlConnection(SQLConnection.GetSqlConnectionString());
            sqlConn.Open();
        }

        public override bool HandleMassge(JObject message)
        {
            bool flag = true;
            string deviceId = message.GetValue("device_id").Value<string>();
            long currentTime = GetCurrentUtcTimeInSeconds();

            CachedHygrometer hygrometer = null;
            if (mHygrometers.ContainsKey(deviceId))
            {
                hygrometer = mHygrometers[deviceId];
            }
            else
            {
                hygrometer = new CachedHygrometer(deviceId, cache, sqlConn);
                mHygrometers[deviceId] = hygrometer;
            }

            // Update current humidity if needed.
            long msgUtcTime = GetUtcTimeOffset(DateTime.Parse(message.GetValue("EventEnqueuedUtcTime").Value<string>()));
            if (Math.Abs(msgUtcTime - currentTime) <= TIME_ACCEPTABLE_DELTA)
                hygrometer.CurrentValue = message.GetValue("humidity").Value<double>();

            hygrometer.Push(msgUtcTime, message.GetValue("humidity").Value<double>());

            return flag;
        }
        
        private class CachedHygrometer : Sensor
        {
            private string deviceId;
            private IDatabase cache;
            private SqlConnection sqlConn;
            
            public double CurrentValue
            {
                get
                {
                    return (double)cache.StringGet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_CURRENT_VALUE, Types.TelemetryType.TT_Humidity));
                }

                internal set
                {
                    currentValue = value;
                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_CURRENT_VALUE, Types.TelemetryType.TT_Humidity), currentValue);
                }
            }

            public CachedHygrometer(string deviceId, IDatabase cache, SqlConnection sqlConn) : base()
            {
                this.deviceId = deviceId;
                this.cache = cache;
                this.sqlConn = sqlConn;

                if (cache.StringGet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_LAST_TIME_POINT, 
                    Types.TelemetryType.TT_Humidity)) == RedisValue.Null)
                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_LAST_TIME_POINT, 
                        Types.TelemetryType.TT_Humidity), lastTimePoint);
                if (cache.StringGet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_CURRENT_VALUE, 
                    Types.TelemetryType.TT_Humidity)) == RedisValue.Null)
                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_CURRENT_VALUE, 
                        Types.TelemetryType.TT_Humidity), currentValue);
                if (cache.StringGet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_PENDING_AVERAGE_VALUE, 
                    Types.TelemetryType.TT_Humidity)) == RedisValue.Null)
                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_PENDING_AVERAGE_VALUE, 
                        Types.TelemetryType.TT_Humidity), pendingAverageValue);
                if (cache.StringGet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_PENDING_COUNT, 
                    Types.TelemetryType.TT_Humidity)) == RedisValue.Null)
                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_PENDING_COUNT, 
                        Types.TelemetryType.TT_Humidity), pendingCount);
            }

            public void Push(long msgUtcTime, double value)
            {
                lastTimePoint = (long)cache.StringGet(RedisCacheConnection.GetCacheKey(
                    deviceId, SUFFIX_LAST_TIME_POINT, Types.TelemetryType.TT_Humidity));
                pendingAverageValue = (double)cache.StringGet(RedisCacheConnection.GetCacheKey(
                    deviceId, SUFFIX_PENDING_AVERAGE_VALUE, Types.TelemetryType.TT_Humidity));
                pendingCount = (int)cache.StringGet(RedisCacheConnection.GetCacheKey(
                    deviceId, SUFFIX_PENDING_COUNT, Types.TelemetryType.TT_Humidity));

                // The temperature value should be deprecated if it's emmitted time is 
                // event earlier than last time point.
                if (msgUtcTime < lastTimePoint)
                    return;

                if (msgUtcTime - lastTimePoint <= TIME_POINT_SPAN)
                {
                    pendingAverageValue = (pendingAverageValue * pendingCount + value) /
                        (pendingCount + 1);
                    pendingCount++;

                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_PENDING_AVERAGE_VALUE,
                        Types.TelemetryType.TT_Humidity), pendingAverageValue);
                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_PENDING_COUNT,
                        Types.TelemetryType.TT_Humidity), pendingCount);
                }
                else
                {
                    if (pendingCount > 0)
                    {
                        using (var dbCommand = sqlConn.CreateCommand())
                        {

                            dbCommand.CommandText = @"
                                INSERT INTO Humidity
                                VALUES ('" + deviceId + "', '" + pendingAverageValue + "', '" + lastTimePoint + "');";

                            dbCommand.ExecuteNonQuery();
                        }
                    }

                    pendingCount = 1;
                    pendingAverageValue = value;
                    lastTimePoint = msgUtcTime / TIME_POINT_SPAN * TIME_POINT_SPAN;

                    // Update values in cache.
                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_LAST_TIME_POINT,
                        Types.TelemetryType.TT_Humidity), lastTimePoint);
                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_PENDING_AVERAGE_VALUE,
                        Types.TelemetryType.TT_Humidity), pendingAverageValue);
                    cache.StringSet(RedisCacheConnection.GetCacheKey(deviceId, SUFFIX_PENDING_COUNT,
                        Types.TelemetryType.TT_Humidity), pendingCount);
                }
            }
        }
    }
}
