﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelemetryWorkerRole.handler
{
    interface ITelemetryHandler
    {
        bool HandleMassge(JObject message);
    }
}