﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Microsoft.WindowsAzure.Storage.Table;
using MetaDataWorkerRole.handler;
using IoTAppBackend.Models;
using System.Diagnostics;

namespace MetaDataWorkerRole.handler
{
    class DeviceInfoHandler : MetaDataHandlerInterface
    {
        private HandlerCore mCore;
        
        public DeviceInfoHandler(HandlerCore core)
        {
            this.mCore = core;
        }

        public bool HandleMassge(JObject message)
        {
            // The DeviceInfoHandler just check if there is a record for current 
            // DeviceInfo report message. If not, insert it into the "DeviceInfo"
            // table in the Azure table storage.
            bool flag = false;
            try
            {
                TableEntity entity = new DeviceInfoTableEntity(
                    message.GetValue("device_id").Value<string>(),
                    message.GetValue("device_key").Value<string>(),
                    message.GetValue("device_type").Value<string>(),
                    message.GetValue("device_location").Value<string>(),
                    message.GetValue("device_description").Value<string>(),
                    message.GetValue("firmware_version").Value<string>(),
                    message.GetValue("hardware_version").Value<string>(),
                    message.GetValue("manufacturer").Value<string>(),
                    message.GetValue("serialnumber").Value<string>(),
                    message.GetValue("default_max_period").Value<string>(),
                    message.GetValue("extra").Value<string>());
                TableOperation insertOperation = TableOperation.Insert(entity);
                mCore.deviceInfo.Execute(insertOperation);
                flag =  true;
            }
            catch (Exception e)
            {
                Trace.TraceInformation(e.ToString());
            }

            return flag;
        }
    }
}
