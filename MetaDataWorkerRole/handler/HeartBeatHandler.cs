﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Microsoft.WindowsAzure.Storage.Table;
using IoTAppBackend.Models;
using System.Diagnostics;

namespace MetaDataWorkerRole.handler
{
    class HeartBeatHandler : MetaDataHandlerInterface
    {
        private HandlerCore mCore;

        public HeartBeatHandler()
        { }

        public HeartBeatHandler(HandlerCore core)
        {
            this.mCore = core;
        }

        public bool HandleMassge(JObject message)
        {
            bool flag = false;

            try
            {
                TableOperation retrieveOpertion = TableOperation.Retrieve<DeviceInfoTableEntity>(
                    DeviceInfoTableEntity.GLOBAL_PARTITION_KEY, message.GetValue("device_id").Value<string>());
                DeviceInfoTableEntity updateEntity =  (DeviceInfoTableEntity)mCore.deviceInfo.Execute(retrieveOpertion).Result;
                if (updateEntity != null)
                {
                    updateEntity.ConnectionState = DeviceInfoTableEntity.CONNECTION_STATE_CONNECTED;
                    TableOperation updateOperation = TableOperation.Replace(updateEntity);
                    mCore.deviceInfo.Execute(updateOperation);
                    flag = true;
                }
            }
            catch (Exception e)
            {
                Trace.TraceInformation(e.ToString());
            }

            return flag;
        }
    }
}
