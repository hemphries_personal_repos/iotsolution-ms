﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetaDataWorkerRole.handler
{
    interface MetaDataHandlerInterface
    {
        bool HandleMassge(JObject message);
    }
}
