﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json.Linq;

namespace MetaDataWorkerRole.handler
{
    class HandlerCore
    {
        private const string endpointSuffix = "core.chinacloudapi.cn";
        private const string storageAccountName = "iotappstorage";
        private const string storageAccountKey = "M22PRG/1dhmOviftfddeSY/SsHkTO1kZ4aSYQxPAkoSF2YKUQzEULIygmKrFh40Qwbmu" +
            "9rzMLrArSedxSvFlpg==";

        private CloudStorageAccount storageAccount;
        private CloudTableClient tableClient;
        internal CloudTable deviceInfo;

        public HandlerCore()
        {
            if (storageAccount == null)
            {
                string storageConnectionString = string.Format("DefaultEndpointsProtocol=https;AccountName={0};" +
                    "AccountKey={1};EndpointSuffix={2}", storageAccountName, storageAccountKey, endpointSuffix);
                storageAccount = CloudStorageAccount.Parse(storageConnectionString);
            }
            if (tableClient == null)
                tableClient = storageAccount.CreateCloudTableClient();

            deviceInfo = tableClient.GetTableReference("DeviceInfo");
            deviceInfo.CreateIfNotExists();
        }
    }
}
