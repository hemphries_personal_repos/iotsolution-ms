﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ServiceBus.Messaging;
using System.Diagnostics;

using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json.Linq;

using IoTAppBackend.Util;
using MetaDataWorkerRole.handler;

namespace MetaDataWorkerRole
{
    class MetaDataProcessor : IEventProcessor
    {
        Stopwatch checkpointStopWatch;

        private HandlerCore mCore;
        private DeviceInfoHandler mDeviceInfoHandler;
        private HeartBeatHandler mHeartBeatHandler;

        async Task IEventProcessor.CloseAsync(PartitionContext context, CloseReason reason)
        {
            Console.WriteLine("Processor Shutting Down. Partition '{0}', Reason: '{1}'.", context.Lease.PartitionId, reason);
            if (reason == CloseReason.Shutdown)
            {
                await context.CheckpointAsync();
            }
        }

        Task IEventProcessor.OpenAsync(PartitionContext context)
        {
            Console.WriteLine("MetaDataProcessor initialized.  Partition: '{0}', Offset: '{1}'",
                context.Lease.PartitionId, context.Lease.Offset);
            this.checkpointStopWatch = new Stopwatch();
            this.checkpointStopWatch.Start();

            this.mCore = new HandlerCore();
            this.mDeviceInfoHandler = new DeviceInfoHandler(this.mCore);
            this.mHeartBeatHandler = new HeartBeatHandler(this.mCore);
            
            return Task.FromResult<object>(null);
        }

        async Task IEventProcessor.ProcessEventsAsync(PartitionContext context, IEnumerable<EventData> messages)
        {
            foreach (EventData eventData in messages)
            {
                string data = Encoding.UTF8.GetString(eventData.GetBytes());
                
                try
                {
                    JObject obj = JObject.Parse(data);
                    int msgType = Int32.Parse(obj.GetValue("message_type").ToString());
                    if (msgType == (int)Types.MessageType.MT_DeviceInfo)
                    {
                        mDeviceInfoHandler.HandleMassge(obj);
                    }
                    else if (msgType == (int)Types.MessageType.MT_HeartBeat)
                    {
                        mHeartBeatHandler.HandleMassge(obj);
                    }
                }
                catch (Exception e)
                {
                    Trace.TraceInformation(e.ToString());
                }
            }

            //Call checkpoint every 5 minutes, so that worker can resume processing from 5 minutes back if it restarts.
            if (this.checkpointStopWatch.Elapsed > TimeSpan.FromMinutes(5))
            {
                await context.CheckpointAsync();
                this.checkpointStopWatch.Restart();
            }
        }
    }
}
