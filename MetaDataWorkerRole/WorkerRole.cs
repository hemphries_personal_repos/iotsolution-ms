using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.ServiceBus.Messaging;

namespace MetaDataWorkerRole
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        private EventProcessorHost eventProcessorHost;

        private const string eventHubConnectionString = 
            "Endpoint=sb://iotappsb.servicebus.chinacloudapi.cn/;SharedAccessKeyName=eventhubowner;" +
            "SharedAccessKey=vkbuo57sz9ywkq4XUIRNhNUADzvMfnJAbtloI5yH9iE=";
        private const string eventHubName = "metadataeh";

        // An undocumented property!
        private const string endpointSuffix = "core.chinacloudapi.cn";
        private const string storageAccountName = "iotappstorage";
        private const string storageAccountKey = "M22PRG/1dhmOviftfddeSY/" +
            "SsHkTO1kZ4aSYQxPAkoSF2YKUQzEULIygmKrFh40Qwbmu9rzMLrArSedxSvFlpg==";

        public override void Run()
        {
            Trace.TraceInformation("MetaDataWorkerRole is running");

            try
            {
                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation("MetaDataWorkerRole has been started");

            string storageConnectionString = string.Format("DefaultEndpointsProtocol=https;AccountName={0};" +
                "AccountKey={1};EndpointSuffix={2}", storageAccountName, storageAccountKey, endpointSuffix);

            eventProcessorHost = new EventProcessorHost(RoleEnvironment.CurrentRoleInstance.Id, eventHubName,
                EventHubConsumerGroup.DefaultGroupName, eventHubConnectionString, storageConnectionString);

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("MetaDataWorkerRole is stopping");

            eventProcessorHost.UnregisterEventProcessorAsync().Wait();
            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("MetaDataWorkerRole has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            var options = new EventProcessorOptions();
            options.ExceptionReceived += (sender, e) => { Trace.TraceInformation(e.Exception.ToString()); };
            await eventProcessorHost.RegisterEventProcessorAsync<MetaDataProcessor>(options);

            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("Working");
                await Task.Delay(1000);
            }
        }
    }
}
